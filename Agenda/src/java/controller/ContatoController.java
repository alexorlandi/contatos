package controller;

import bean.ContatoBean;
import dao.ContatoDao;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

/**
 *
 * @author Alex
 */
@ManagedBean
@SessionScoped
public class ContatoController implements Serializable{
    
    private ContatoBean contatoB;
    private DataModel listaContatos;

    public ContatoController() {
    
        contatoB = new ContatoBean();
        
    }

    public ContatoBean getContatoB() {
        return contatoB;
    }

    public void setContatoB(ContatoBean contatoB) {
        this.contatoB = contatoB;
    }

    public DataModel getListaContatos() {
        ContatoDao cD = new ContatoDao();
        listaContatos = new ListDataModel(cD.listarContato());
        return listaContatos;
    }

    public void setListaContatos(DataModel listaContatos) {
        this.listaContatos = listaContatos;
    }
    
    public void novoContato(){
        contatoB = new ContatoBean();
    }
    
    public void selecionarContato(){
        contatoB = (ContatoBean)listaContatos.getRowData();
    }
    
    public String salvaContato(){
        ContatoDao cD = new ContatoDao();
        if(cD.salvarContato(contatoB)){
            FacesContext contexto = FacesContext.getCurrentInstance();
            contexto.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Cadastrado com sucesso!", ""));
            return "listarcontatos";
        }return "erro";
    }
    
    public String editarContato(){
        ContatoDao cD = new ContatoDao();
        if(cD.editarContato(contatoB)){
            FacesContext contexto = FacesContext.getCurrentInstance();
            contexto.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Editado com sucesso!", ""));
            return "listarcontatos";
        }return "erro";
    }
    
    public String excluirContato(){
        ContatoDao cD = new ContatoDao();
        if(cD.excluirContato(contatoB)){
            FacesContext contexto = FacesContext.getCurrentInstance();
            contexto.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Excluído com sucesso!", ""));
            return "listarcontatos";
        }return "erro";
    }

}
