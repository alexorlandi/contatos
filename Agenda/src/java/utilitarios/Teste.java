package utilitarios;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Teste {
    
    public static void main (String[] args){
        
        try {
            Conexao con = new Conexao();
            PreparedStatement pst = con.getConnection().prepareStatement("select * from contato");
            ResultSet rs = pst.executeQuery();
            rs.next();
            System.out.println(rs.getString("nome"));
            System.out.println(rs.getString("email"));
        } catch (SQLException ex) {
            Logger.getLogger(Teste.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
