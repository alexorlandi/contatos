package dao;

//@author Alex 
import bean.ContatoBean;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utilitarios.Conexao;

public class ContatoDao {

    private Conexao con;

    public ContatoDao() {
        con = new Conexao();
    }

    public boolean salvarContato(ContatoBean contato) {
        try {
            String sql = "insert into contato (nome, endereco, cidade, uf, telefone, celular, email) values(?, ?, ?, ?, ?, ?, ?);";

            PreparedStatement pst = con.getConnection().prepareStatement(sql);
            pst.setString(1, contato.getNome());
            pst.setString(2, contato.getEndereco());
            pst.setString(3, contato.getCidade());
            pst.setString(4, contato.getUf());
            pst.setString(5, contato.getTelefone());
            pst.setString(6, contato.getCelular());
            pst.setString(7, contato.getEmail());

            pst.execute();
            con.getConnection().commit();           
            
            return true;            
        } catch (SQLException ex) {
            Logger.getLogger(ContatoDao.class.getName()).log(Level.SEVERE, null, ex + "Erro ao salvar os dados!");
        }return false;
    }
        
   public boolean editarContato(ContatoBean contato) {
        try {
            String sql = "update contato set nome = ?, endereco = ?, cidade = ?, uf = ?, telefone = ?, celular = ?, email = ? where id = ?;";

            PreparedStatement pst = con.getConnection().prepareStatement(sql);
            pst.setString(1, contato.getNome());
            pst.setString(2, contato.getEndereco());
            pst.setString(3, contato.getCidade());
            pst.setString(4, contato.getUf());
            pst.setString(5, contato.getTelefone());
            pst.setString(6, contato.getCelular());
            pst.setString(7, contato.getEmail());
            pst.setInt(8, contato.getId());

            pst.execute();
            con.getConnection().commit();

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ContatoDao.class.getName()).log(Level.SEVERE, null, ex + "Erro ao editar os dados!");
        }return false;
    }
   
public boolean excluirContato(ContatoBean contato) {
        try {
            String sql = "delete from contato where id = ?;";

            PreparedStatement pst = con.getConnection().prepareStatement(sql);
            
            pst.setInt(1, contato.getId());

            pst.execute();
            con.getConnection().commit();

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ContatoDao.class.getName()).log(Level.SEVERE, null, ex + "Erro ao editar os dados!");
        }return false;
    } 

public List<ContatoBean> listarContato() {        
    List<ContatoBean> lista = new ArrayList<ContatoBean>();
        
        try {
            String sql = "select * from contato;";

            PreparedStatement pst = con.getConnection().prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            
            while (rs.next()) {
                ContatoBean contato = new ContatoBean();
            
                contato.setId(rs.getInt("id"));
                contato.setNome(rs.getString("nome"));
                contato.setEndereco(rs.getString("endereco"));
                contato.setCidade(rs.getString("cidade"));
                contato.setUf(rs.getString("uf"));
                contato.setTelefone(rs.getString("telefone"));
                contato.setCelular(rs.getString("celular"));
                contato.setEmail(rs.getString("email"));
                lista.add(contato);              
            }            
        } catch (SQLException ex) {
            Logger.getLogger(ContatoDao.class.getName()).log(Level.SEVERE, null, ex + "Erro ao editar os dados!");
        }return lista;
    }
   
}